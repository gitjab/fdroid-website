---
layout: page
title: Issues
permalink: /issues/
---
If you’re having a problem with the F-Droid client application,
please check if we already know about it, and/or report it,
in [this issue tracker](https://gitlab.com/fdroid/fdroidclient/issues).

For problems relating to the contents of the repository,
such as missing or outdated applications,
please use [this issue tracker](https://gitlab.com/fdroid/fdroiddata/issues).

For the server tools (used for building apps yourself,
or running your own repo), please use 
[this issue tracker](https://gitlab.com/fdroid/fdroidserver/issues).

For issues relating to this web site, you can use
[this issue tracker](https://gitlab.com/fdroid/fdroid-website/issues).
